<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'quotes_wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/0XJB4~U{qtsJeJZ=W}SgMd}kA9<($]w4QsOkU(r(KazwSTB:#;EVm)=6G7Y0d-1');
define('SECURE_AUTH_KEY',  '2OIP?s1kh5Af;dxAC|C7>Mm-Tk0^ytu#0!o#TOVXLyeSd1-23uRXkKz~:.2flSBs');
define('LOGGED_IN_KEY',    '+^<D>sxr3&QD@p?~(zO?;9$clvf.25 }b);]*_iI]Sz+t[?a$*n<5/<lv0[Cq==x');
define('NONCE_KEY',        'l0NpcxcXm+J3Ow]+~~dkS`M. 5rOC98R*nP|KE4;uHSj4WwK%J%]{g7#WRGp:Z@|');
define('AUTH_SALT',        'xO(Oe0Y&vs*-d~9k@}~{e&#_O>Yks=,=[*]BzV>_>h(GWcjwxoLR7sta(S`YcR6u');
define('SECURE_AUTH_SALT', 'ZRkUQz!tk%CQ:[GCc<T7,hqhO:9_mUneGJIi%nd2J!W.vc:/+RZLLfjZwzj|dfGP');
define('LOGGED_IN_SALT',   '|!FJlp]6g[[x!(93O38)|;6c^g^ph{pP,H}[a6Yk];wD%f*/4}+pAb+P&E*:84Rc');
define('NONCE_SALT',       '4z(bey^+&p<_hm6(ER^X.1pX26@18h!F3$*%&~Z< F/U&hT|)9@<LBFF;iOCiNM:');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
