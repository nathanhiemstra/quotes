<? 
    $quote_context = $post->post_excerpt;
    $quote_date = get_field('date'); 
    $quote_count = count(get_field('quote_and_name')); 
    $quote_content_main = get_the_content();
    $quote_author_main_first = get_the_author_meta( 'first_name' );
    $quote_author_main_last = get_the_author_meta( 'last_name' );
    $quote_author_main_username = get_the_author_meta( 'user_nicename' );
    $quote_author_main = $quote_author_main_first." ".$quote_author_main_last;
    $quote_author_main_link = '<a href="/author/'.$quote_author_main_username.'" class="quote-subtle-link">'.$quote_author_main.'</a>';
    
  ?>


  <div class="quote-date">
    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="quote-subtle-link"><?php the_time( get_option( 'date_format' ) ); ?></a>
  </div>


  <?php 
    // SHOW MAIN POST AS FIRST OF DIALOG QUOTES
    if( have_rows('quote_and_name') ): 
  ?>
    
    <dl class="quote-item quote-multi">
      <dt class="quote-author"><?=$quote_author_main_link?>: </dt>
      <dd class="quote-content"><?=$quote_content_main?></dd>
    </dl>

    <?php 
      // LOOP THROUGH DIALOG QUOTES
      while( have_rows('quote_and_name') ): the_row(); 
        $quote_content = get_sub_field('quote');
        $quote_author_info  = get_sub_field('author-user');
        $quote_author_first_name = $quote_author_info['user_firstname'];
        $quote_author_last_name = $quote_author_info['user_lastname'];
        $quote_author_username = $quote_author_info['user_nicename'];
        $quote_author = $quote_author_first_name." ".$quote_author_last_name;
        $quote_author_link = '<a href="/author/'.$quote_author_username.'" class="quote-subtle-link">'.$quote_author.'</a>';


    ?>


      <dl class="quote-item quote-multi">
        <dt class="quote-author"><?=$quote_author_link?>: </dt>
        <dd class="quote-content"><?=$quote_content?></dd>
      </dl>

    <?php endwhile; ?>

    <? // CONTEXT ?>
    <? if ($quote_context) : ?>
      <div class="entry-context ">&ndash; <?=$quote_context?></div>
    <? endif ?>


  <? else : ?>
    <?  // SHOW SINGLE QUOTE ?>
    <dl class="quote-item quote-single">
      <dt class="quote-content"><?=$quote_content_main?></dt>
      <dd class="quote-author">
        <? 
          if ($quote_context) : 
            $quote_context_single_quote = ', <span class="entry-context">' . $quote_context . '</span>'; 
          endif 
        ?>
        &ndash; <?=$quote_author_main_link.$quote_context_single_quote?>
      </dd>
    </dl>

  <? endif; ?>